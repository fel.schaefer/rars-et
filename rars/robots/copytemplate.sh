#!/bin/bash
# Erzeugt die Robot-cpps aus der Vorlage
for n in `seq -w 0 60`; do
	cp et-se-template.cpp robot$n.cpp
	sed -i 's/RobotXX/Robot'"$n"'/g' robot$n.cpp
done
