/**
 * Vorlage für den Robot im Software-Projekt
 *
 * @author    Ingo Haschler <lehre@ingohaschler.de>
 * @version   et12
 */

//--------------------------------------------------------------------------
//                           I N C L U D E
//--------------------------------------------------------------------------

#include "car.h"
#include <math.h>

//--------------------------------------------------------------------------
//                           Class Robot05
//--------------------------------------------------------------------------

class Robot05 : public Driver
{
public:
    // Konstruktor
    Robot05()
    {
        // Der Name des Robots
        m_sName = "Robot05";
        // Namen der Autoren
        m_sAuthor = "Felix Schäfer";
        // Hier die vorgegebenen Farben eintragen
        m_iNoseColor = oBLACK;
        m_iTailColor = oMAGENTA;
        m_sBitmapName2D = "car_red_black";
        // Für alle Gruppen gleich
        m_sModel3D = "futura";
    }

    const static double REIB  = 0.95;        //Maximaler Reibungskoeffizient
    const static double VERZOEGERUNG = 50.0; //Verzögerung beim Bremsen

    double Bremsweg (double v0, double v1, double verzoegerung)
    {
        if (v1 >= v0)                               //Wenn die Zielgeschw. größergleich als die Startgeschw. ist
        {
            return 0.0;                             //Bremsweg gleich null -> Geschw. halten oder beschleunigen
        }

        double dv = v0 - v1;
        return (v0 + 0.5 * dv) * dv / verzoegerung;
    }

    /** Berechnet die maximale Geschwindigkeit für ein Streckenelement
    *   Parameter:  in double radius Radius des Streckenelements in Fuß. Radius = 0.0 bedeutet Gerade    *
    *   Rückgabe: double Maximalgeschwindigkeit in Fuß pro Sekunde
    */
    double Geschwindigkeit (double radius, double spur)
    {
        if (radius < 0.0)
        {
            radius = -radius;
        }

        if (radius == 0.0)      //Auf Geraden Vollgas geben
        {
            return 250;
        }
        return sqrt((radius + spur) * 32.2 * REIB);
    }

    con_vec drive(situation& s)
    {
        // hier wird die Logik des Robots implementiert
        con_vec result = CON_VEC_EMPTY;

        double breite = s.to_lft + s.to_rgt;
        double spur = breite / 2.0;

        //result.vc = 80;                                     //mit konstanter Geschwindigkeit fahren

        double geschw = Geschwindigkeit(s.cur_rad, spur);
        double n_geschw = Geschwindigkeit(s.nex_rad, spur);

        double to_end;
        if (s.cur_rad == 0)
        {
            to_end = s.to_end;
        }
        else
        {
            to_end = 2.0 * PI / s.to_end * (s.cur_rad + spur);
        }

        if (to_end <= Bremsweg(s.v, n_geschw, VERZOEGERUNG))
        {
            //bremsen
            if (s.v > n_geschw)
            {
                result.vc = 0.95 * s.v;
            }
            else
            {
                result.vc = 1.01 * s.v;
            }
        }
        else
        {
            if (s.v > geschw)//geschwindigkeit halten
            {
                result.vc = 0.99 * s.v;
            }
            else
            {
                result.vc = 1.50 * s.v;
            }
        }

        result.alpha = -1 * (PI / 2.0 - acos(s.vn / s.v)) + 0.01 * (s.to_lft - spur);  //Lenkwinkel = Winkel um den die Geschwindigkeit von der Geschwindigkeit parallel zur Strecke abweicht

        if (s.damage > 29000 || s.fuel < 10)
        {
            result.request_pit = 1;
            result.fuel_amount = 150.0 - s.fuel;
            result.repair_amount = s.damage;
        }

        return result;
    }
};

/**
 * Diese Methode darf nicht verändert werden.
 * Sie wird vom Framework aufgerufen, um den Robot zu erzeugen.
 * Der Name leitet sich (wie der Klassenname) von der Gruppenbezeichnung ab.
 */
Driver * getRobot05Instance()
{
    return new Robot05();
}
