/**
 * Vorlage für den Robot im Software-Projekt
 *
 * @author    Ingo Haschler <lehre@ingohaschler.de>
 * @version   et12
 */

//--------------------------------------------------------------------------
//                           I N C L U D E
//--------------------------------------------------------------------------

#include "car.h"
#include <math.h>

//--------------------------------------------------------------------------
//                           Class Robot04
//--------------------------------------------------------------------------

class Robot04 : public Driver
{
public:
    // Konstruktor
    Robot04()
    {
        // Der Name des Robots
        m_sName = "Robot04";
        // Namen der Autoren
        m_sAuthor = "";
        // Hier die vorgegebenen Farben eintragen
        m_iNoseColor = oBLACK;
        m_iTailColor = oMAGENTA;
        m_sBitmapName2D = "car_red_black";
        // Für alle Gruppen gleich
        m_sModel3D = "futura";
    }
    //Konstanten
    const static double REIB  = 0.9;        //Maximaler Reibungskoeffizient
    const static double VERZOEGERUNG = 30.0; //Verzögerung beim Bremsen
    const static double LENKFAKTOR = 0.5;
    const static double DAEMPHUNGSFAKTOR = 1.1;
    const static double BIG_SLIP = 9.0;

    /** Berechnet den benötigten Bremsweg, der gebraucht wird um mit einer bestimmten Verzoegerung vom der Geschwindigkeit v0 auf die Geschw. v1 abzubremsen
    *   Parameter:  in double v0 Startgeschwindigkeit in Fuß pro Sekunde
    *               in double v1 Zielgeschwindigkeit in Fuß pro Sekunde
    *               in doble verzoegerung Verzoegerung in Fuß pro Sekunde im Quadrat
    *   Rückgabe: double Bremsweg in Fuß
    */
    double Bremsweg (double v0, double v1, double verzoegerung)
    {
        if (v1 >= v0)                               //Wenn die Zielgeschw. größergleich als die Startgeschw. ist
        {
            return 0.0;                             //Bremsweg gleich null -> Geschw. halten oder beschleunigen
        }

        double dv = v1 - v0;
        return (v0 + .5 * dv) * dv / verzoegerung;
    }

    /** Berechnet die maximale Geschwindigkeit für ein Streckenelement
    *   Parameter:  in double radius Radius des Streckenelements in Fuß. Radius = 0.0 bedeutet Gerade    *
    *   Rückgabe: double Maximalgeschwindigkeit in Fuß pro Sekunde
    */
    double Geschwindigkeit (double radius)
    {
        if (radius == 0.0)      //Auf Geraden Vollgas geben
        {
            return 250;
        }

        return sqrt(radius * 32.2 * REIB);
    }

    /** Berechnet den benötigten Lenkwinkel
    *   Parameter:  in situation &s Zeiger auf die Situationsparameter des Rennwagens
    *               in double spur angestrebter Abstand zum linken Streckenreand
                    in double geschw angestrebte Geschwindigkeit auf aktuellem Streckenelement
    *   Rückgabe: double Lenkwinkel im Bogenmaß
    */
    double Lenkwinkel(situation &s, double spur, double geschw)
    {
        double bias;
        if (s.cur_rad == 0.0 )
            bias = 0.0;
        else
            bias = (s.v*s.v/(geschw*geschw)) * atan(BIG_SLIP / geschw);

        if (s.cur_rad < 0.0)
            bias = -bias;

        return LENKFAKTOR * (s.to_lft - spur) - DAEMPHUNGSFAKTOR * s.vn;
    }

    con_vec drive(situation& s)
    {
        // hier wird die Logik des Robots implementiert
        con_vec result = CON_VEC_EMPTY;
        double breite = s.to_lft + s.to_rgt;
        double spur = breite/2.0;

        double geschw = Geschwindigkeit(s.cur_rad + spur);
        double n_geschw = Geschwindigkeit(s.nex_rad + spur);



        double to_end;
        if (s.cur_rad == 0)
        {
            to_end = s.to_end;
        }
        else
        {
            to_end = 2.0 * PI / s.to_end * (s.cur_rad + spur);
        }

        if (s.to_end <= Bremsweg(s.v, n_geschw, VERZOEGERUNG))
        {
            result.vc = n_geschw;       //bremsen
        }
        else
        {
            result.vc = geschw;         //geschwindigkeit halten
        }

        result.alpha = Lenkwinkel(s, spur, geschw);

        return result;
    }
};

/**
 * Diese Methode darf nicht verändert werden.
 * Sie wird vom Framework aufgerufen, um den Robot zu erzeugen.
 * Der Name leitet sich (wie der Klassenname) von der Gruppenbezeichnung ab.
 */
Driver * getRobot04Instance()
{
    return new Robot04();
}
